#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: colored2 4.0.0 ruby lib

Gem::Specification.new do |s|
  s.name = "colored2".freeze
  s.version = "4.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.metadata = { "rubygems_mfa_required" => "true" } if s.respond_to? :metadata=
  s.require_paths = ["lib".freeze]
  s.authors = ["Chris Wanstrath".freeze, "Konstantin Gredeskoul".freeze]
  s.date = "2023-08-24"
  s.description = "This is a heavily modified fork of http://github.com/defunkt/colored gem, with many\nsensible pull requests combined. Since the authors of the original gem no longer support it,\nthis might, perhaps, be considered a good alternative.\n\nSimple gem that adds various color methods to String class, and can be used as follows:\n\n  require 'colored2'\n\n  puts 'this is red'.red\n  puts 'this is red with a yellow background'.red.on.yellow\n  puts 'this is red with and italic'.red.italic\n  puts 'this is green bold'.green.bold << ' and regular'.green\n  puts 'this is really bold blue on white but reversed'.bold.blue.on.white.reversed\n  puts 'this is regular, but '.red! << 'this is red '.yellow! << ' and yellow.'.no_color!\n  puts ('this is regular, but '.red! do\n    'this is red '.yellow! do\n      ' and yellow.'.no_color!\n    end\n  end)\n\n".freeze
  s.email = "kigster@gmail.com".freeze
  s.files = ["LICENSE".freeze, "README.md".freeze, "Rakefile".freeze, "lib/colored2.rb".freeze, "lib/colored2/ascii_decorator.rb".freeze, "lib/colored2/codes.rb".freeze, "lib/colored2/numbers.rb".freeze, "lib/colored2/object.rb".freeze, "lib/colored2/strings.rb".freeze, "lib/colored2/version.rb".freeze]
  s.homepage = "http://github.com/kigster/colored2".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 3.1".freeze)
  s.rubygems_version = "3.3.15".freeze
  s.summary = "Add even more color to your life.".freeze
end
